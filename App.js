import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Person from './Person/Person';


class App extends Component {

  state = {
    persons: [
      {id:'1', name: 'Sunil', age: 26 },
      {id:'2', name: 'Anil', age: 27 },
      {id:'23', name: 'GiriBabu', age: 28 }
    ],
    showPersons: false
  }

  
  nameChangedHandler = (event) => {
    this.setState({
      persons: [
        { name: 'Maxximusns', age: 26 },
        { name: event.target.value, age: 27 },
        { name: 'GiriBabu', age: 32 }
      ]
    })
  }

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  }

  deletePersonHandler = (personIndex) => {
    //const persons = this.state.persons.slice();
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons: persons})
  }
  render() {

    const style = {
      backgroundColor: 'green',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer'
    }

    let persons = null;

    if (this.state.showPersons) {

      
      persons = (
        <div>
        
          {
            this.state.persons.map(
              (person, index) => {
                return <Person 
                click={() => this.deletePersonHandler(index)}
                name={person.name}
                age={person.age} 
                key={person.id}
                />
              }
            )
          }
        </div>
      );

      
      style.backgroundColor= 'red'
    }

    return (
      <div className="container">
        <h1>Hello Sunil !</h1>
        <button style={style}
          onClick={this.togglePersonsHandler}>Switch Name</button>
        {persons}
      </div>
    );
  }
}

export default App;
